extern crate dotenv;

use dotenv::dotenv;

use comelec::configuration::get_configuration;
use comelec::run;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>>
{
    dotenv().ok();

    let configuration = get_configuration().unwrap();
    run(&configuration).await.unwrap();

    Ok(())
}
