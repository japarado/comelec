pub enum ApplicationStatus
{
    DatabaseError(sqlx::Error),
    HttpError(reqwest::Error),
    Success,
}
