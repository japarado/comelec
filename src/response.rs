use std::collections::HashMap;

use serde::de::DeserializeOwned;
use serde::Deserialize;

#[allow(unused)]
pub const BASE_URL: &str = "https://comelec.gov.ph/2019NLEResults";
#[allow(unused)]
pub const BASE_URL_PRECINCT: &str = "https://comelec.gov.ph/2019NLEResults/data/results";

#[derive(Debug, Default, Deserialize, Clone)]
pub struct RegularResponse
{
    pub rc: i32,
    pub rn: String,
    pub url: String,
    pub can: String,
    pub srs: Option<HashMap<i32, RegularResponse>>,
}

#[derive(Debug, Default, Deserialize)]
pub struct BarangayResponse
{
    pub rc: i32,
    pub rn: String,
    pub url: String,
    pub can: String,
    pub pps: Option<Vec<BarangayPrecinctsResponsePart>>,
}

#[derive(Debug, Default, Deserialize, Clone)]
pub struct BarangayPrecinctsResponsePart
{
    pub ppc: i32,
    pub ppn: String,
    pub vbs: Option<Vec<BarangayPrecinctResponsePart>>,
}

#[derive(Debug, Default, Deserialize, Clone)]
pub struct BarangayPrecinctResponsePart
{
    pub vbc: i32,
    pub url: String,
}

#[derive(Debug, Default, Deserialize)]
pub struct PrecinctResponse
{
    pub vbc: i32,
    pub cos: Vec<PrecinctResponseAttributePart>,
}

#[derive(Debug, Default, Deserialize)]
pub struct PrecinctResponseAttributePart
{
    pub cn: String,
    pub ct: i32,
}

pub enum ResponseType
{
    Regular,
    Barangay,
    Precinct,
}

pub async fn fetch_from_comelec<T: DeserializeOwned>(
    url: &str,
    response_type: ResponseType,
) -> Result<T, reqwest::Error>
{
    match response_type
    {
        ResponseType::Regular =>
        {
            reqwest::get(format!("{}/data/regions/{}.json", BASE_URL, url))
                .await?
                .json::<T>()
                .await
        }
        ResponseType::Barangay =>
        {
            reqwest::get(format!("{}/data/regions/{}.json", BASE_URL, url))
                .await?
                .json::<T>()
                .await
        }
        ResponseType::Precinct =>
        {
            reqwest::get(format!("{}/data/results/{}.json", BASE_URL, url))
                .await?
                .json::<T>()
                .await
        }
    }
}

pub trait Parent
{
    type Child;

    fn get_children(&self) -> Option<Vec<Self::Child>>;
}

impl Parent for RegularResponse
{
    type Child = RegularResponse;

    fn get_children(&self) -> Option<Vec<Self::Child>>
    {
        self.clone()
            .srs
            .map(|children| children.into_iter().map(|(_, value)| value).collect())
    }
}

impl Parent for BarangayResponse
{
    type Child = BarangayPrecinctsResponsePart;

    fn get_children(&self) -> Option<Vec<Self::Child>>
    {
        self.pps.as_ref().map(|child| child.to_vec())
    }
}
