use std::time;

use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;

use crate::configuration::DatabaseSettings;

pub async fn create_connection_pool(db_config: &DatabaseSettings) -> Result<PgPool, sqlx::Error>
{
    let conn = PgConnectOptions::new()
        .host(db_config.host.as_str())
        .port(db_config.port)
        .username(db_config.username.as_str())
        .password(db_config.password.as_str())
        .database(db_config.database_name.as_str());
    PgPoolOptions::new()
        .connect_timeout(time::Duration::from_secs(2))
        .connect_with(conn)
        .await
}
