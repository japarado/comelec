use async_recursion::async_recursion;
use sqlx::PgPool;

use crate::configuration::Settings;
use crate::error::ApplicationStatus;
use crate::startup::create_connection_pool;

pub mod configuration;
pub mod database;
pub mod error;
pub mod models;
pub mod response;
pub mod startup;

pub async fn run(config: &Settings) -> anyhow::Result<ApplicationStatus>
{
    let pool = create_connection_pool(&config.database).await?;
    process(&pool).await?;
    Ok(ApplicationStatus::Success)
}

#[async_recursion]
async fn process(_pool: &PgPool) -> anyhow::Result<ApplicationStatus>
{
    Ok(ApplicationStatus::Success)
}
