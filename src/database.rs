use sqlx::{PgPool, Postgres, Transaction};

// TODO: remove unwrap()
pub async fn reset(pool: &PgPool)

{
    let mut transaction = start_transaction(pool).await;
    sqlx::query!("DELETE FROM country")
        .execute(&mut transaction)
        .await
        .unwrap();
    commit_transaction(transaction).await;
    println!("Deleted all the countries");
}

// TODO: remove unwrap()
pub async fn start_transaction(pool: &PgPool) -> Transaction<'_, Postgres>
{
    pool.begin()
        .await
        .map_err(|e| panic!("Error creating transaction {:?}", e))
        .unwrap()
}

pub async fn commit_transaction(transaction: Transaction<'_, Postgres>)
{
    // TODO: remove unwrap()
    transaction
        .commit()
        .await
        .map_err(|e| panic!("Error finishing transaction {:?}", e))
        .unwrap()
}
