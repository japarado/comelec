use std::fmt::Debug;

use async_trait::async_trait;
use sqlx::{Error, PgPool};

#[derive(Debug, Default, Clone)]
pub struct Country
{
    pub id: i64,
    pub name: String,
    pub url: String,
}

#[derive(Debug, Default, Clone)]
pub struct Region
{
    pub id: i64,
    pub name: String,
    pub url: String,
    pub country_id: i64,
}

#[derive(Debug, Default, Clone)]
pub struct Province
{
    pub id: i64,
    pub name: String,
    pub url: String,
    pub region_id: i64,
}

#[derive(Debug, Default, Clone)]
pub struct City
{
    pub id: i64,
    pub name: String,
    pub url: String,
    pub province_id: i64,
}

#[derive(Debug, Default, Clone)]
pub struct Barangay
{
    pub id: i64,
    pub name: String,
    pub url: String,
    pub city_id: i64,
}

#[derive(Debug, Default, Clone)]
pub struct Precinct
{
    pub id: i64,
    pub name: String,
    pub url: String,
    pub barangay_id: i64,
}

impl Country
{
    pub fn new(id: i64, name: String, url: String) -> Self
    {
        Self { id, name, url }
    }
}

impl Region
{
    pub fn new(id: i64, name: String, url: String, country_id: i64) -> Self
    {
        Self {
            id,
            name,
            url,
            country_id,
        }
    }
}

impl Province
{
    pub fn new(id: i64, name: String, url: String, region_id: i64) -> Self
    {
        Self {
            id,
            name,
            url,
            region_id,
        }
    }
}

impl City
{
    pub fn new(id: i64, name: String, url: String, province_id: i64) -> Self
    {
        Self {
            id,
            name,
            url,
            province_id,
        }
    }
}

impl Barangay
{
    pub fn new(id: i64, name: String, url: String, city_id: i64) -> Self
    {
        Self {
            id,
            name,
            url,
            city_id,
        }
    }
}

impl Precinct
{
    pub fn new(id: i64, name: String, url: String, barangay_id: i64) -> Self
    {
        Self {
            id,
            name,
            url,
            barangay_id,
        }
    }
}

#[async_trait]
trait Model
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized;
    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized;
}

#[async_trait]
trait CrudResource
{
    async fn get(&self, pool: &PgPool, id: i64) -> Result<Self, Error>
    where
        Self: Sized;
}

#[async_trait]
impl<T: Model + std::marker::Sync + std::marker::Send> CrudResource for T
{
    async fn get(&self, pool: &PgPool, id: i64) -> Result<Self, Error>
    where
        Self: Sized,
    {
        self.refresh(pool, Some(id)).await
    }
}

#[async_trait]
impl Model for Country
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized,
    {
        sqlx::query!(
            "INSERT INTO country (id, name, url) VALUES ($1, $2, $3)",
            self.id,
            self.name,
            self.url
        )
        .execute(pool)
        .await?;

        self.refresh(pool, None).await
    }

    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        let id = if let Some(id) = id { id } else { self.id };
        sqlx::query_as!(Self, "SELECT * FROM country WHERE id = $1", id)
            .fetch_one(pool)
            .await
    }
}

#[async_trait]
impl Model for Region
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized,
    {
        sqlx::query!(
            "INSERT INTO region (id, name, url, country_id) VALUES ($1, $2, $3, $4)",
            self.id,
            self.name,
            self.url,
            self.country_id
        )
        .execute(pool)
        .await?;

        self.refresh(pool, None).await
    }

    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        let id = if let Some(id) = id { id } else { self.id };
        sqlx::query_as!(Self, "SELECT * FROM region WHERE id = $1", id)
            .fetch_one(pool)
            .await
    }
}

#[async_trait]
impl Model for Province
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized,
    {
        sqlx::query!(
            "INSERT INTO province (id, name, url, region_id) VALUES ($1, $2, $3, $4)",
            self.id,
            self.name,
            self.url,
            self.region_id
        )
        .execute(pool)
        .await?;

        self.refresh(pool, None).await
    }

    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        let id = if let Some(id) = id { id } else { self.id };
        sqlx::query_as!(Self, "SELECT * FROM province WHERE id = $1", id)
            .fetch_one(pool)
            .await
    }
}

#[async_trait]
impl Model for City
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized,
    {
        sqlx::query!(
            "INSERT INTO city (id, name, url, province_id) VALUES ($1, $2, $3, $4)",
            self.id,
            self.name,
            self.url,
            self.province_id
        )
        .execute(pool)
        .await?;

        self.refresh(pool, None).await
    }

    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        let id = if let Some(id) = id { id } else { self.id };
        sqlx::query_as!(Self, "SELECT * FROM city WHERE id = $1", id)
            .fetch_one(pool)
            .await
    }
}

#[async_trait]
impl Model for Barangay
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized,
    {
        sqlx::query!(
            "INSERT INTO barangay (id, name, url, city_id) VALUES ($1, $2, $3, $4)",
            self.id,
            self.name,
            self.url,
            self.city_id
        )
        .execute(pool)
        .await?;

        self.refresh(pool, None).await
    }

    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        let id = if let Some(id) = id { id } else { self.id };
        sqlx::query_as!(Self, "SELECT * FROM barangay WHERE id = $1", id)
            .fetch_one(pool)
            .await
    }
}

#[async_trait]
impl Model for Precinct
{
    async fn save(&self, pool: &PgPool) -> Result<Self, Error>
    where
        Self: Sized,
    {
        sqlx::query!(
            "INSERT INTO precinct (id, name, url, barangay_id) VALUES ($1, $2, $3, $4)",
            self.id,
            self.name,
            self.url,
            self.barangay_id
        )
        .execute(pool)
        .await?;

        self.refresh(pool, None).await
    }

    async fn refresh(&self, pool: &PgPool, id: Option<i64>) -> Result<Self, Error>
    where
        Self: Sized,
    {
        let id = if let Some(id) = id { id } else { self.id };
        sqlx::query_as!(Self, "SELECT * FROM precinct WHERE id = $1", id)
            .fetch_one(pool)
            .await
    }
}
