use serde::Deserialize;
use serde_aux::field_attributes::deserialize_number_from_string;

#[derive(Debug, Deserialize)]
pub struct Settings
{
    pub database: DatabaseSettings,
}

#[derive(Debug, Deserialize)]
pub struct DatabaseSettings
{
    pub username: String,
    pub password: String,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub port: u16,
    pub host: String,
    pub database_name: String,
}

impl DatabaseSettings
{
    pub fn form_connection_string(&self) -> String
    {
        format!(
            "postgres://{}:{}@{}:{}/{}",
            self.username, self.password, self.host, self.port, self.database_name
        )
    }
}

impl From<Settings> for DatabaseSettings
{
    fn from(settings: Settings) -> Self
    {
        Self {
            username: settings.database.username,
            password: settings.database.password,
            port: settings.database.port,
            host: settings.database.host,
            database_name: settings.database.database_name,
        }
    }
}

pub fn get_configuration() -> Result<Settings, config::ConfigError>
{
    let mut settings = config::Config::default();
    let base_path = std::env::current_dir().expect("Failed to determine the current directory");
    let configuration_directory = base_path.join("configuration");

    settings
        .merge(config::File::from(configuration_directory.join("base")).required(true))
        .unwrap();
    settings
        .merge(config::Environment::with_prefix("app").separator("__"))
        .unwrap();
    settings.try_into()
}

pub fn form_connection_string(db_config: &DatabaseSettings) -> String
{
    format!(
        "postgres://{}:{}@{}:{}/{}",
        db_config.username,
        db_config.password,
        db_config.host,
        db_config.port,
        db_config.database_name
    )
}
