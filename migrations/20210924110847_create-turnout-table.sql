-- Add migration script here
CREATE TABLE turnout
(
    id              INT PRIMARY KEY,
    expected_voters INT NOT NULL,
    actual_voters   INT NOT NULL,
    precinct_id     INT NOT NULL,
    CONSTRAINT fk_province
        FOREIGN KEY (precinct_id)
            REFERENCES precinct (id)
            ON DELETE CASCADE
);
